@extends('app')
@section('title')
<title>Registration</title>
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel = "stylesheet" type = "text/css" href = "css/registration.css">
<script type = "text/javascript" src = "http://localhost/SampleLaravel/public/js/registerValidate.js"></script>
@stop

@section('content')
<div class = "page-header">
    <h2>Registration</h2>
</div>
<div class="col-sm-6 col-sm-offset-4">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form id = "register" method = "POST" action = "">
        {{ csrf_field() }}
        <div class="form-group">
            <!--<i class = "glyphicon glyphicon-user"></i>-->
            <!--<label for = "Name">Name</label><br>-->
            <input type = "text" class = "form-control" id = "userName" name = "userName" maxlength = "20" placeholder = "name"/>
        </div>
        <div class="form-group">
            <!--<i class="glyphicon glyphicon-envelope"></i>-->
            <!--<label for = "email">Email</label><br>-->
            <input type = "email" class = "form-control" id = "email" name = "email" maxlength = "64" placeholder = "e-mail"/>
            <span id = "emailError"></span>
        </div>
        <div class="form-group">
            <!--<i class = "glyphicon glyphicon-lock"></i>-->
            <!--<label for = "password">Password</label><br>-->
            <input type = "password" class = "form-control" id = "password" name = "password" maxlength = "20" placeholder = "password"/>
        </div>
        <div class="form-group">
            <!--<i class = "glyphicon glyphicon-lock"></i>-->
            <!--<label for = "confirmPassword">Confirm Password</label><br>-->
            <input type = "password" class = "form-control" id = "password_confirmation" name = "password_confirmation" maxlength = "20" placeholder = "confirm password"/>
        </div>
        <div class="col-md-12 col-md-auto">
            <button type = "submit" class = "btn btn-success btn-md col-md-6" name = "register" id = "signUp" value = "Register">Register</button>
            <!--<button type = "reset" class = "btn btn-danger btn-md col-md-4">Reset</button>-->
            <button type = "button" class = "btn btn-info btn-md col-md-6" name = "Home" id = "Home" onclick = "window.location ='{{ url('login') }}'" >Home</button>
        </div>
        <div>
        </div>
    </form>
</div>
@stop
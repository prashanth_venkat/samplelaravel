@extends('app')
@section('title')
<title>Address List</title>
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel = "stylesheet" type = "text/css" href = "{{ URL('css/address.css') }}">
<script type = "text/javascript" src = "http://localhost/SampleLaravel/public/js/addressValidate.js"></script>
@stop

@section('content')
<div class = "page-header">
    <h2>Address Book</h2>
</div> 
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="list">List Address</a></li>
                <li><a href="add">Add Address</a></li>
                <li><a href=" {{ url('logout') }}">Logout</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class = "active"><a class="glyphicon glyphicon-user">&nbsp;Welcome {{ Auth::user()->user_name }}</a></li>
            </ul> 
        </div>
    </div>
</nav>
<div class = "container-fluid ">
    <table class = "table table-hover" id = "list">
        <thead class = "thead-inverse">
        <span class = "Error "></span>
        <tr>
            <th></th>
            <th>Name</th>
            <th>Address</th>
            <th>Mobile</th>
            <th>Edit</th>
        </tr>
        </thead>
        @if (!empty($addressList))
        @for ($iteration = 0; $iteration<$count; $iteration++)
        <tr>
            <td><input type="checkbox" id = "boxes" class = "messageCheckbox" class = "checkbox-primary" name = "selected[]" value = "{{ $addressList[$iteration]['address_id'] }}" data-toggle = "tooltip" data-placement = "top" title = "Select to delete the addresses">&nbsp;</td>
            <td>{{ $addressList[$iteration]['name'] }}</td>
            <td>{{ $addressList[$iteration]['address'].', '.$addressList[$iteration]['city'].', '.$addressList[$iteration]['state_name'].', '.$addressList[$iteration]['country_name'] }}</td>
            <td>{{ $addressList[$iteration]['mobile'] }}</td>
            <td><input type = "button" class="btn btn-info" value = "Edit" id = "edit" name = "edit" onclick = "window.location ='{{ url("address/edit/".$addressList[$iteration]['address_id']) }}'" data-toggle = "tooltip" data-placement = "top" title = "Edit current address"></td>
        </tr>
        @endfor
        @else
        <tr>
            <td colspan = "5" align = "center" style = "word-spacing: 10px">No address added yet</td>
        </tr>
        @endif
    </table>
    @if (!empty($addressList))
    <button id = "delete" class="btn btn-danger" name = "delete" onclick = "deleteRecords()" disabled>Delete Selected</button>
    @endif
</div>
<script>
    $('.messageCheckbox').change(function () {
        if ($('.messageCheckbox:checked').length) {
            $('#delete').removeAttr('disabled');
        } else {
            $('#delete').attr('disabled', 'disabled');
        }
    });
</script>
@stop

@extends('app')
@section('title')
<title>Add Address</title>
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel = "stylesheet" type = "text/css" href = "{{ URL('css/address.css') }}">
<script type = "text/javascript" src = "http://localhost/SampleLaravel/public/js/addressValidate.js"></script>
@stop

@section('content')
<div class = "page-header">
    <h2>Add Details</h2>
</div>
<nav class = "navbar navbar-inverse">
    <div class = "container-fluid">
        <ul class = "nav navbar-nav">
            <li><a href = "{{ url('address/list') }}">List Address</a></li>
            <li class = "active"><a href = "./add">Add Address</a></li>
            <li><a href = "{{ url('logout') }}">Logout</a></li>
        </ul>
        <ul class = "nav navbar-nav navbar-right">
            <li class = "active"><a class = "glyphicon glyphicon-user">&nbsp;Welcome {{ Auth::user()->user_name }}</a></li>
        </ul>
    </div>
</nav>
@if ($errors->any())
<div>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<form id = "addAddress" method = "post" action = "">
    {{ csrf_field() }}
    <table class = "table table-condensed">
        <tr>
            <td>Name</td>
            <td>
                <input type = "text" class = "form-control" id = "name" name = "name" maxlength = "20">
                <span class = "Error" id = "nameError"></span>
            </td>
        </tr>
        <tr>
            <td>Age</td>
            <td>
                <select id = "age" class = "form-control" name = "age">
                    <option></option>
                    @for ($iteration=1; $iteration<=100; $iteration++)
                    <option value = "{{ $iteration }}">{{ $iteration }}</option>
                    @endfor
                </select>
                <span class = "Error" id = "ageError"></span>
            </td>
        </tr>
        <tr>
            <td>Address &nbsp; &nbsp; </td>
            <td>
                <textarea id = "address" class = "form-control" name = "address" rows = "4" cols = "20" maxlength = "120"></textarea>
                <span class = "Error" id = "addressError"></span>
            </td>
        </tr>
        <tr>
            <td>Mobile</td>
            <td>
                <input type = "text" class = "form-control" id = "mobile" name = "mobile" maxlength = "13">
                <span class = "Error" id = "mobileError"></span>
            </td>
        </tr>
        <tr>
            <td>Country</td>
            <td>
                <select id = "country_id" class = "form-control" name = "country_id" onchange = "dropdownRequest()">
                    <option></option>
                    @foreach ($countryList as $result)
                    <option value = "{{ $result->country_id }}">{{ $result->country_name }}</option>
                    @endforeach
                </select>
                <span class = "Error" id = "countryError"></span>
            </td>
        </tr>
        <tr>
            <td>State</td>
            <td>
                <select id = "state_id" class = "form-control" name = "state_id" >
                    <option></option>
                </select>
                <span class = "Error" id = "stateError"></span>
            </td>
        </tr>
        <tr>
            <td>City</td>
            <td>
                <input type = "text" class = "form-control" id = "city" name = "city" maxlength = "20">
                <span class = "Error" id = "cityError"></span>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <button type = "submit" class="btn btn-success" name = "addAddress" id = "addAddress" value = "Add Address">Add</button>
                <input type = "reset" class="btn btn-warning" onclick = "resetSpan()">
                <input type = "button" class = "btn btn-danger" name = "cancel" id = "cancel" value = "Cancel" onclick = "window.location ='{{ url('address/list') }}'">
            </td>
        </tr>
    </table>
</form>
@stop
@extends('app')
@section('title')
<title>About</title>
@stop
@section('content')
    <h1>About Me</h1>
    <h4>Name: {{ $first }} {{ $last}} </h4>
    @if (count($hobbies))
        <h4>Hobbies: </h4>
        <ul>
            @foreach ($hobbies as $hobby)
            <li> {{ $hobby}}</li>
            @endforeach
        </ul>
    @endif
@stop
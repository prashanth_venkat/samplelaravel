@extends('app')

@section('title')
<title>Login</title>
@stop

@section('js')
<link rel = "stylesheet" type = "text/css" href = "css/login.css">
<script type = "text/javascript" src = "http://localhost/SampleLaravel/public/js/loginValidate.js"></script>
@stop

@section('content')
<div class = "page-header">
    <h2>Login</h2>
</div>
@if (session('message'))
<div class = "alert alert-success">
    {{ session('message') }}
</div>
@endif
<div class = "col-sm-6 col-sm-offset-4">
    @if ($errors->any())
    <div class = "alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form id = "login-form" name = "login" method = "post" action = "">
        {{ csrf_field() }}
        <div class = "form-group">
            <!--<i class = "glyphicon glyphicon-envelope"></i>-->
            <!--<label for = "email">Email</label><br>-->
            <input type = "email" class = "form-control" id = "email" name = "email" placeholder = "e-mail"/>
        </div>
        <div class = "form-group">
            <!--<i class = "glyphicon glyphicon-lock"></i>-->
            <!--<label for = "password">Password</label><br>-->
            <input type = "password" class = "form-control" id = "password" name = "password" placeholder = "password"/>
        </div>
        <div class="col-md-12 col-md-auto">
            <button type = "submit" class = "btn btn-success btn-md col-md-6"  name = "login" id = "login" value = "Login">Login</button>
            <!--<button type = "reset" class = "btn btn-danger" >Reset</button>-->
            <!--&nbsp;&nbsp;New User? &nbsp;-->
            <button type = "button" class = "btn btn-info btn-md col-md-6" name = "signUp" id = "signUp" value = "Sign Up" onclick = "window.location ='{{ url('register') }}'">Sign Up</button></p>
            <!--New User?&nbsp;<a href = "{{ url('register') }}">Sign up</a>-->
        </div>
    </form>
</div>
@stop
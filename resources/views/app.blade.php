<!DOCTYPE html>
<html lang = "en">
    <head>
        <meta charset = "UTF-8">
        <meta name = "viewport" content = "width=device-width, initial-scale = 1.0">
        <link rel = "icon" href = "/images/address_book.ico">
        @yield('title')
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
        @yield('js')
<!--        <script type="text/javascript">
            function noBack() {
                window.history.forward();
            }
            noBack();
            window.onload = noBack;
            window.onpageshow = function (event) {
                if (event.persisted)
                    noBack();
            };
            window.onunload = function () {
                void(0);
            };
        </script>-->
    </head>
    <body>
        <div class = "container">
            @yield('content')
        </div>
    </body>
</html>
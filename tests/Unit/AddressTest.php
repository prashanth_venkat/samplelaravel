<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Address;

class AddressTest extends TestCase
{

    /**
     * To check list of addresses of current user
     */
    public function testGetList()
    {
        $addressModel = new Address(2);
        $output = $addressModel->getList();
        $expected = array(
            0 => array(
                "address_id" => 5,
                "name" => "Okja",
                "address" => "Hill top",
                "mobile" => "1111111",
                "user_id" => 2,
                "city" => "Seoul",
                "country_name" => "USA",
                "state_name" => "New Jersey"
                )
        );
        $this->assertEquals($expected, $output);
    }
    /**
     * To check retrieval of address
     */
    public function testGetAddress()
    {
        $addressModel = new Address(5);
        $output = $addressModel->getAddress(2);
        $expected = array(
            0 => array(
                "address_id" => 5,
                "name" => "Okja",
                "address" => "Hill top",
                "mobile" => "1111111",
                "user_id" => 2,
                "city" => "Seoul",
                "age" => 11,
                "country_name" => "USA",
                "country_id" => 2,
                "state_name" => "New Jersey",
                "state_id" => 101
            )
        );
        $this->assertEquals($expected, $output);
    }
    /**
     * To check validation of the address details provided
     */
    public function testValidate()
    {
        $addressDetails = array(
            "name" => "Okja",
            "age" => "11",
            "address" => "Hill top",
            "mobile" => "74562888787",
            "country_id" => "2",
            "state_id" => "101",
            "city" => "Seoul",
        );
        $addressModel = new Address($addressDetails);
        $output = $addressModel->validate();
        $this->assertTrue($output);
    }
    /**
     * To check storing of address details
     */
//    public function testStore()
//    {
//        $addressDetails = array(
//            "_token" => "VjAL1Jr6nU3uGZ45jO9lxpwqqLJPg0FqmPlIDh4L",
//            "name" => "Hank Schrader",
//            "age" => "46",
//            "address" => "222 b Baker St",
//            "mobile" => "4657987541654",
//            "country_id" => "2",
//            "state_id" => "101",
//            "city" => "Seoul",
//            "addAddress" => "Add Address"
//        );
//        $addressModel = new Address($addressDetails);
//        $output = $addressModel->store(7);
//        $this->assertTrue($output);
//    }
    /**
     * To check updating of address details
     */
    public function testUpdateAddress()
    {
        $addressDetails = array(
            "_token" => "VjAL1Jr6nU3uGZ45jO9lxpwqqLJPg0FqmPlIDh4L",
            "name" => "Hank Schrader",
            "age" => "46",
            "address" => "222 b Baker St",
            "mobile" => "7987541654",
            "country_id" => "2",
            "state_id" => "101",
            "city" => "Seoul",
            "updateAddress" => "Update Address"
            );
        $addressModel = new Address($addressDetails);
        $output = $addressModel->updateAddress(27,7);
    }
    
    /**
     * To check delete operation
     */
    public function testDeleteAddress()
    {
        $addressModel = new Address();
        $output = $addressModel->deleteAddress(26);
        $this->assertFalse($output);
    }
}

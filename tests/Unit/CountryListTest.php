<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Services\CountryList;

class CountryListTest extends TestCase
{
    /**
     * To check countries list
     */
    public function testGetList()
    {
        $output = CountryList::getList()->toArray();
        $expected = array(
            0 => array(
                "country_id" => 1,
                "country_name" => "INDIA"
            ),
            1 => array(
                "country_id" => 2,
                "country_name" => "USA"
            )
        );
        $this->assertEquals($expected, $output);
    }
}

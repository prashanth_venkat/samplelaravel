<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Services\AddressList;
use App\User;
use Illuminate\Support\Facades\Bus;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class AddressControllerTest extends TestCase
{

    public function __construct() {
//        $this->addressService = new AddressList;
        $this->user = new User(array(
            'name' => 'Prashanth V',
            ));
    }
    /**
     * To check list operation
     */
    public function testShow()
    {
//        $addressService = new AddressList;
//        $addressList = $this->addressService->getList(2);
        $output = AddressList::getList(2);
        $expected = array(
            0 => array(
                "address_id" => 5,
                "name" => "Okja",
                "address" => "Hill top",
                "mobile" => "1111111",
                "user_id" => 2,
                "city" => "Seoul",
                "country_name" => "USA",
                "state_name" => "New Jersey"
            )
        );
        $this->assertEquals($expected, $output);
    }

    /**
     * To check delete operation
     */
    public function testDelete()
    {
        $this->be($this->user);
        $response = $this->call('POST', 'delete', ["address_id" => 7]);
        $response->assertStatus(200);
    }
    /**
     * To check address list page
     */
    public function testListPage()
    {
        $this->be($this->user);
        $response = $this->call('GET', 'address/list');
        $this->assertEquals(200, $response->status());
    }
    
    /**
     * To check add page
     */
    public function testGetAddPage()
    {
        $this->be($this->user);
        $response = $this->call('GET', 'address/add');
        $this->assertEquals(200, $response->status());
    }
    
    /**
     * To check invalid add address
     */
    public function testInvalidAddAddress()
    {
        $this->be($this->user);
        $response = $this->call('POST', 'address/add', [
            "_token" => "VjAL1Jr6nU3uGZ45jO9lxpwqqLJPg0FqmPlIDh4L",
            "name" => "Okja",
            "age" => "11",
            "address" => "top",
            "mobile" => "745621329987",
            "country_id" => "2",
            "state_id" => "101",
            "city" => "Seoul",
            "addAddress" => "Add Address"
            ]);
        $expected = "The address must be at least 8 characters.";
        $actual = session()->all()['errors']->all()[0];
        $this->assertEquals($expected, $actual);
        $response->assertRedirect('address/add');
        $this->assertEquals(302, $response->status());
    }

    /**
     * To check update operation with invalid details
     */
    public function testInvalidUpdate()
    {
        $this->be($this->user);
        $response = $this->call('POST', 'address/edit/4', [
            "_token" => "njfDSYoG3T2cEGnCEKD097kShk3AJTajLin09B2p",
            "name" => "Okja",
            "age" => "11",
            "address" => "Hill top",
            "mobile" => "1111111",
            "country_id" => "2",
            "state_id" => "101",
            "city" => "Seoul",
            "updateAddress" => "Update Address"
            ]);
        $expected = "The mobile must be at least 10 characters.";
        $actual = session()->all()['errors']->all()[0];
        $this->assertEquals($expected, $actual);
        $response->assertRedirect('address/edit/4');
        $this->assertEquals(302, $response->status());
    }
}

<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\AuthModel;
use App\Http\Controllers\AuthController;

class StubTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $authModel = $this->getMockBuilder(AuthModel::class)
                ->disableOriginalConstructor()
                ->getMock();
        $authModel->expects($this->once())
                ->method('checkExistence')
                ->will($this->returnValue(true));
        $this->assertEquals(true, $authModel->checkExistence());
    }
    
    public function testLogin()
    {
        $authController = $this->getMockBuilder(AuthController::class)
                ->getMock();
        $authController->expects($this->any())
                ->method('logout')
                ->willReturn("43");
    }
    
//    public function testDetailsAreValid()
//    {
//        $args = array(
//            "email" => "thara@gmail.com",
//            "password" => "Thara@123",
//        );
//        $authModel = $this->getMockBuilder(AuthModel::class)
//                ->setMethods(['validateLogin'])
//                ->setConstructorArgs($args)
////                ->disableOriginalConstructor()
//                ->getMock();
////        dd($authModel);
////        $authModel->expects($this->once())
////                ->method('validateLogin');
//        
////        $authController = new AuthController();
////        $request = array(
////            "email" => "thara@gmail.com",
////            "password" => "Thara@123",
////        );
////        $authController->login($request);
//        
//        $authController = $this->getMockBuilder(AuthController::class)
//                ->setMethods(['login'])
//                ->getMock();
////        $authController->expects($this->once())
////                ->method('login');
//        
//        
//    }
}
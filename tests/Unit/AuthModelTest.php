<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \App\AuthModel;

class AuthModelTest extends TestCase
{
    private $authModel;
    private $db;
    public function setUp()
    {
        parent::setUp();
        $this->authModel = new AuthModel();
//        $this->authModel = $this->getMockBuilder(AuthModel::class)
//                ->getMock();                
        
    }
    
    public function testCheckExistence()
    {
        $emailList = array(
            "venkataraman@gmail.com",
            "prashanth@gmail.com",
            "thara@gmail.com",
            "uma@gmail.com"
        );
        $needle = "thara@gmail.com";
        $result = in_array($needle, $emailList);
        $this->assertTrue($result);
    }
    
    public function testCheckInexistence()
    {
        $emailList = array(
            "venkataraman@gmail.com",
            "prashanth@gmail.com",
            "thara@gmail.com",
            "uma@gmail.com"
        );
        $needle = "jimmy.page@gmail.com";
        $result = in_array($needle, $emailList);
        $this->assertFalse($result);
    }

    public function testVerify()
    {
        
    }
//
//    /**
//     * Check storing of user details
//     */
//    public function testStoreUser()
//    {
//        $userDetails = array(
//            'userName' => 'John Denver',
//            'email' => 'johnDenver@gmail.com',
//            'password' => 'John@123',
//            'password_confirmation' => 'John@123',
//        );
//        $authModel = new AuthModel($userDetails);
//        $response = $authModel->store();
//        $this->assertFalse($response);
//    }
//    
//    /**
//     * To verify the user existence
//     */
//    public function testVerifyUserExistence()
//    {
//        $loginDetails = array(
//            'email' => 'thara@gmail.cm',
//            'password' => 'Thara@123'
//        );
//        $authModel = new AuthModel($loginDetails);
//        $response = $authModel->verify();
//        $this->assertFalse($response);
//    }
//    /**
//     * To check registration details validation
//     */
//    public function testValidateRegistration()
//    {
//        $userDetails = array(
//            'userName' => 'Prashanth V',
//            'email' => 'prashanth@gmail.com',
//            'password' => 'Prashanth@123',
//            'password_confirmation' => 'Prashanth@123',
//            );
//        $authModel = new AuthModel($userDetails);
//        $response = $authModel->validateRegistration();
//        $this->assertTrue($response);
//    }
//    /**
//     * To check login details validation
//     */
//    public function testValidateLogin()
//    {
//        $loginDetails = array(
//            'email' => 'thara@gmail.com',
//            'password' => 'Thara@123'
//        );
//        $authModel = new AuthModel($loginDetails);
//        $response = $authModel->validateLogin();
//        $this->assertTrue($response);
//    }
//    /**
//     * To check user existence
//     */
//    public function testCheckExistence()
//    {
//        $email = "thara@gmail.com";
//        $authModel = new AuthModel($email);
//        $response = $authModel->checkExistence();
//        $this->assertTrue($response);
//    }
    
    public function testVerifyValidUser()
    {
        $loginDetails = array(
            "email" => "chuck@gmail.com",
            "password" => "Chuck@123"  
        );
        $authModel = new AuthModel($loginDetails);
        $result = $authModel->verify();
        $this->assertInstanceOf(AuthModel::class, $result);
    }
    
    public function testVerifyInvalidUser()
    {
        $loginDetails = array(
            "email" => "chuck@gmail.com",
            "password" => "Chuck@12"  
        );
//        $stub = $this->getMockBuilder(AuthModel::class)
//                ->setConstructorArgs($loginDetails)
//                ->setMethods(['verify'])
//                ->getMock();
//        $stub->expects($this->once())
//                ->method('verify')
//                ->willReturn(false);
        $authModel = new AuthModel($loginDetails);
        $result = $authModel->verify();
        $this->assertFalse($result);
    }
    
    public function testValidateCorrectRegistrationDetails()
    {
        
        $registrationDetails = array(
            "userName" => "Robert Plant",
            "email" => "robert@gmail.com",
            "password" => "Robert@123",
            "password_confirmation" => "Robert@123"
        );
        $authModel = new AuthModel($registrationDetails);
        $result = $authModel->validateRegistration();
        $this->assertTrue($result);
    }
    
    public function testValidateIncorrectRegistrationDetails()
    {
        
        $registrationDetails = array(
            "userName" => "Robert Plant",
            "email" => "robert@gmail.com",
            "password" => "Robert123",
            "password_confirmation" => "Robert123"
        );
        $expected = array (
            "password" => array(
                0 => "The password format is invalid."
            )
        );
        $authModel = new AuthModel($registrationDetails);
        $result = $authModel->validateRegistration();
        $this->assertEquals($expected, $result->getMessages());
    }
    
    public function testValidateCorrectLogin()
    {
        $loginDetails = array(
            "email" => "thara@gmail.com",
            "password" => "Thara@123"
        );
        $authModel = new AuthModel($loginDetails);
        $result = $authModel->validateLogin();
        $this->assertTrue($result);
    }
    
    public function testValidateIncorrectLogin()
    {
        $loginDetails = array(
            "email" => "thara@gmailcom",
            "password" => "Thara@123"
        );
        $expected = array(
            "email" => array(
                0 => "The email must be a valid email address."
            )
        );
        $authModel = new AuthModel($loginDetails);
        $result = $authModel->validateLogin();
        $this->assertEquals($expected, $result->getMessages());
    }
    
    public function testUserExistence()
    {
        $email = "thara@gmail.com";
        $authModel = new AuthModel($email);
        $result = $authModel->checkExistence();
        $this->assertTrue($result);
    }
    
    public function testUserInexistence()
    {
        $email = "tharaaa@gmail.com";
        $authModel = new AuthModel($email);
        $result = $authModel->checkExistence();
        $this->assertFalse($result);
    }
    
    public function testStoreWrongDetails()
    {
        $registrationDetails = array(
            "userName" => "thara",
            "email" => "thara@gmail.com",
            "password" => "Thara@123",
            "password_confirmation" => "Thara@123"
        );
        $authModel = new AuthModel($registrationDetails);
        $result = $authModel->store();
        $this->assertFalse($result);
    }
    
//    public function testStoreCorrectDetails()
//    {
//        $registrationDetails = array(
//            "userName" => "thara",
//            "email" => "thara@gmail.com",
//            "password" => "Thara@123",
//            "password_confirmation" => "Thara@123"
//        );
//        $authModel = new AuthModel($registrationDetails);
//        $result = $authModel->store();
//        $this->assertFalse($result);
//    }
}

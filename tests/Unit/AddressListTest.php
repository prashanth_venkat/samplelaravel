<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Services\AddressList;

class AddressListTest extends TestCase
{
    /**
     * To get address list of user
     */
    public function testGetList()
    {
        
        $output = AddressList::getList(2);
        $expected = array(
            0 => array(
                "address_id" => 5,
                "name" => "Okja",
                "address" => "Hill top",
                "mobile" => "1111111",
                "user_id" => 2,
                "city" => "Seoul",
                "country_name" => "USA",
                "state_name" => "New Jersey"
                )
        );
        $this->assertEquals($expected, $output);
    }
    /**
     * To get particular address
     */
    public function testGetAddress()
    {
        
        $output = AddressList::getAddress(4,3);
        $expected = array(
            0 => array(
            "address_id" => 4,
            "name" => "Okja",
            "address" => "Hill top",
            "mobile" => "1111111",
            "user_id" => 3,
            "city" => "Seoul",
            "age" => 11,
            "country_name" => "USA",
            "country_id" => 2,
            "state_name" => "New Jersey",
            "state_id" => 101
            )
        );
        $this->assertEquals($expected, $output);
    }
}

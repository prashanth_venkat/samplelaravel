<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\User;
use App\Http\Controllers\AuthController;
use App\AuthModel;

class AuthControllerTest extends TestCase
{
//    public function __construct() {
//        $this->user = new User(array(
//            'name' => 'Prashanth V',
//            ));
//    }
//
//    /**
//     * To check logout
//     */
//    public function testLogout()
//    {
//        $response = $this->get('/logout');
//        $this->assertEquals(302, $response->status());
//        $response->assertRedirect('/login');
//    }
//    /**
//     * To check login page with get request
//     */
//    public function testGetLogin()
//    {
//        $response = $this->call('GET', 'login');
//        $this->assertEquals(200, $response->status());
//    }
//    /**
//     * To check login page with get request when authenticated
//     */
//    public function testAuthGetLogin()
//    {
//        $this->be($this->user);
//        $response = $this->call('GET', 'login');
//        $response->assertRedirect('address/list');
//        $this->assertEquals(302, $response->status());
//    }
//    /**
//     * To check validation of login details
//     */
//    public function testValidDetailsLogin()
//    {
//        $response = $this->call('POST', '/login', [
//            'email' => 'thara@gmail.com',
//            'password' => 'Thara@123',
//            ]);
//        $response->assertRedirect('address/list');
//        $this->assertEquals(302, $response->status());
//    }
//    /**
//     * To check validation of login details
//     */
//    public function testInvalidDetailsLogin()
//    {
//        $response = $this->call('POST', '/login', [
//            'email' => 'tharagmail.com',
//            'password' => 'Thara@123',
//            ]);
//        $expected = "The email must be a valid email address.";
//        $actual = session()->all()['errors']->all()[0];
//        $this->assertEquals($expected, $actual);
//        $response->assertRedirect('login');
//        $this->assertEquals(302, $response->status());
//    }
//    /**
//     * To check authenticity of user logging in
//     */
//    public function testInvalidUserLogin()
//    {
//        $response = $this->call('POST', '/login', [
//            'email' => 'tara@gmail.com',
//            'password' => 'Thara@123',
//            ]);
//        $expected = "Email and Password Didn't match";
//        $actual = session()->all()['message'];
//        $this->assertEquals($expected, $actual);
//        $response->assertRedirect('login');
//        $this->assertEquals(302, $response->status());
//    }
//    /**
//     * To check register page with get request
//     */
//    public function testGetRegister()
//    {
//        $response = $this->call('GET', 'register');
//        $this->assertEquals(200, $response->status());
//    }
//    /**
//     * To check register page with get request when authenticated
//     */
//    public function testAuthRegister()
//    {
//        $this->be($this->user);
//        $response = $this->call('GET', 'register');
//        $response->assertRedirect('address/list');
//        $this->assertEquals(302, $response->status());
//    }
//    /**
//     * To check validation of registration details
//     */
//    public function testInvalidDetailsRegister()
//    {
//        $response = $this->call('POST', '/register', [
//            'userName' => 'Prashanth V',
//            'email' => 'prashanth@gmail.com',
//            'password' => 'prashanth@123',
//            'password_confirmation' => 'Prashanth@123',
//            ]);
//        $expected = "The password format is invalid.";
//        $actual = session()->all()['errors']->all()[0];
//        $this->assertEquals($expected, $actual);
//        $this->assertEquals(302, $response->status());
//        $response->assertRedirect('register');
//    }
//    /**
//     * To check valid registration details
//     */
//    public function testValidDetailsRegister()
//    {
//        $response = $this->call('POST', '/register', [
//            'userName' => 'Prashanth V',
//            'email' => 'prashanth@gmail.com',
//            'password' => 'Prashanth@123',
//            'password_confirmation' => 'Prashanth@123',
//            ]);
//        $this->assertEquals(302, $response->status());
//        $response->assertRedirect('login');
//    }
//    
//    /**
//     * To check the availability of email
//     */
//    public function testGetAvailability()
//    {
//        $response = $this->call("POST", "/getAvailability", [
//                'email' => 'thara@gmail.com'
//                ]);
//        $expected = "Already Exists";
//        $this->assertEquals($expected , $response->content());
//        $response->assertStatus(200);
//    }
    
    public function testLogin()
    {
        $args = array(
            'email' => 'thara@gmail.com',
            'password' => 'Thara@123'
        );
//        $stub = $this->getMockBuilder(AuthModel::class)
//                ->setConstructorArgs($args)
//                ->setMethods(['validateLogin'])
//                ->getMock();
//        $stub->expects($this->any())
//                ->method('validateLogin')
//                ->with($this->equalTo(""))
//                ->willReturn(true);
////        dd($this->authModelStub);
//        $authModel = new AuthController();
////        $authModel->login($args);
//        $authController = new AuthController();
//        $result = $authController->login($args);
//        $this->assertTrue($result);
    }
    private $authModelStub;
    
    public function setUp() {
        parent::setUp();
        $this->authModelStub = array(
            'email' => 'thara@gmail.com',
            'password' => 'Thara@123'
        );
    }
}

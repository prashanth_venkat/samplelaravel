<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Users::create(array('email' => 'prasi@gmail.com', 'password' => 'Prasi@123'));
        Users::create(array('email' => 'aarief@gmail.com', 'password' => 'Aarief@123'));
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddressList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address_list', function(Blueprint $table) {
            $table->increments('address_id');
            $table->string('name');
            $table->tinyInteger('age');
            $table->string('address');
            $table->string('mobile');
            $table->string('city');
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('country_id')->on('country');
            $table->integer('state_id')->unsigned();
            $table->foreign('state_id')->references('state_id')->on('state');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

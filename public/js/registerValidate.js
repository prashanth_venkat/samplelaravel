/**
 * To validate registration details
 */
$(document).ready(function() {
    $.validator.addMethod("regex", function(value, element, regexpr) {          
        return regexpr.test(value);
        });
    $('#register').validate({
        rules: {
            userName: {
                required : true,
                minlength: 3,
                maxlength: 20,
                regex    : /^[a-zA-Z ]*$/
            },
            email: {
                required : true,
                email    : true,
                maxlength: 64
            },
            password: {
                required : true,
                minlength: 8,
                maxlength: 20,
                regex    : /^(?=.*[0-9])(?=.*[A-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/
            },
            password_confirmation: {
                required : true,
                equalTo  : "#password"
            }
        },
        messages : {
            userName: {
                required  : "Name is required",
                minlength : "Name should be at least {0} characters",
                maxlength : "Please enter a value less than or equal to {0}",
                regex     : "Please enter a valid name",
            },
            email: {
                required  : "E-mail is required",
                email     : "Please provide valid E-mail",
                maxlength : "Please enter a value less than or equal to {0}",
            },
            password: {
                required : "Password is required",
                minlength: "Password should be at least {0} characters",
                maxlength: "Please enter a value less than or equal to {0}",
                regex    : "Minimum 1 Num,Caps,Spl chars",
            },
            password_confirmation: {
                required : "Confirm password is required",
                equalTo  : "Password mismatch"
            }
        }
    });

//    $('#register input').on('blur', function () {
//        $(this).valid() ? $('button.btn').prop('disabled', false)
//            : $('button.btn').prop('disabled', 'disabled');
//            
//        if(!$(this).validate()) {
//            
//        } else {
//            var error = false;
//            $('#register input').each(function() {
//                if($(this).validate() == false) {
//                    error = true;
//                }
//            })
//        }
//        if(error)
//            $('button.btn').prop('disabled', false);
//    });
    
    $("#email").blur(function() {
        var emailData = $(this).val();
        if (emailData.match(/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/)) {
            $.ajax({
                type    : "POST",
                url     : "getAvailability",
                data    : "email="+emailData,
                datatype: "text",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success : function(text) {
                    $("#emailError").html(text);
                    text === "Already Exists" ? $("#signUp").attr("disabled", true) : $("#signUp").prop('disabled', false);
                    }
            });
        } else {
            $("#signUp").attr("disabled", true);
        }
    });  
});

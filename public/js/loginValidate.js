/**
 * To validate login details
 * @type type
 */
$(document).ready(function() {
    $.validator.addMethod("regex", function(value, element, regexpr) {          
        return regexpr.test(value);
        }, "Provide valid details");
    $('#login-form').validate({
       rules: {
            email: {
                required : true,
                email    : true,
                maxlength: 64,
                regex    : /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/
            },
            password: {
                required : true,
                minlength: 8
            }
       },
       messages : {
           email: {
                required : "E-mail is required",
                maxlength: "Please enter no more than {0} characters",
                regex    : "Please enter valid e-mail"
            },
            password: {
                required : "Password is required",
                minlength: "Please enter a minimum of {0} characters"
            }
       }
    });
});
/**
 * Ajax request to display the states of corresponding country selected
 * 
 * @param {string} state_id
 *
 * @returns {response}
 */
function dropdownRequest(state_id)
{
    var country = $(country_id).val();
    $.ajax({
        type     : "POST",
        url      : "/getstate",
        data     : "country=" + country,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        datatype : "json",
        success : function(text) {
            var jsonData = JSON.parse(text);
            for (var data = 0; data<jsonData.length-1; data++) {
                state_id == jsonData[data].state_id ? $('<option selected/>').val(jsonData[data].state_id).text(jsonData[data].state_name).appendTo('#state_id') : $('<option/>').val(jsonData[data].state_id).html(jsonData[data].state_name).appendTo('#state_id');
            }
        }
    });
    $('#state_id').empty();
}

/**
 * To delete the selected records
 * 
 * @returns {response} and redirect
 */
function deleteRecords()
{
    var result = confirm("Are you sure to delete the selected records?");
    if (result) {
        var address_id = $('input:checkbox:checked').map(function() {
            return this.value;
        }).get();
        console.log(address_id);
        $.ajax({
            type    : "POST",
            url     : "/delete",
            data    : "address_id=" + address_id,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success : function(text) {
                alert("Succesfully Deleted");
                location.href = "/address/list"; 
            }

        });
    }
}

/**
 * To validate address details
 */
$(document).ready(function() {
    $.validator.addMethod("regex", function(value, element, regexpr) {          
        return regexpr.test(value);
    });
    $('#addAddress').validate({
        errorElement: "div",
        errorPlacement: function(error, element) {
            element.after(error);
        },
        rules: {
            name: {
                required : true,
                minlength: 3,
                maxlength: 20,
                regex    : /^[a-z A-Z ]*$/
            },
            age: {
                required: true,
            },
            address: {
                required : true,
                minlength: 8,
                maxlength: 120,
                regex    : /^[a-zA-Z0-9\s,/'-]*$/
            },
            mobile: {
                required : true,
                minlength: 10,
                maxlength: 13,
                regex    : /^[a-z A-Z 0-9 +]*$/
            },
            country_id: {
                required: true,  
            },
            state_id: {
                required: true,  
            },
            city: {
                required  : true,
                minlength : 4,
                maxlength : 20,
                regex     : /^[a-z A-Z ]*$/
            }
        },
        messages: {
            name :{
                required  : "Name is required",
                minlength : "Name should be at least {0} characters",
                maxlength : "Please enter a value less than or equal to {0} for name",
                regex     : "Please enter a valid Name"
            },
            age: {
                required: "Age is required",
            },
            address: {
                required  : "Address is required",
                minlength : "Address should be at least {0} characters",
                maxlength : "Please enter a value less than or equal to {0} for address",
                regex     : "Please provide valid details for address",
            },
            mobile: {
                required  : "Mobile is required",
                minlength : "Mobile should be at least {0} characters",
                maxlength : "Please enter a value less than or equal to {0} for mobile",
                regex     : "Please provide valid details for mobile",
            },
            country_id: {
                required : "Country is required",  
            },
            state_id: {
                required : "State is required",  
            },
            city: {
                required  : "City is required",
                minlength : "City should be at least {0} characters",
                maxlength : "Please enter a value less than or equal to {0} for city",
                regex     : "Please provide valid details for mobile",
            }
        }
    });
});
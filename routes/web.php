<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

//Route::get('/', function () {
//    return view('welcome');
//});

Route::redirect('/', 'login', 301);

Route::get('about', 'PagesController');

Route::get('contact', 'WelcomeController@contact');

Route::group(['middleware' => 'prevent-back-history'], function() {
    
//    Auth::routes();
    
    Route::get('login', 'AuthController@login')->name("login");

    Route::post('login', 'AuthController@login')->name("login");

//Route::post('postLogin', 'PostsController@store');

    Route::get('index', 'UserController@index');

    Route::get('register', 'AuthController@register');

    Route::post('register', 'AuthController@register');

    Route::post('getAvailability', 'AuthController@getAvailability');

    Route::group(['middleware' => 'auth'], function() {
        
        Route::get('address/list', 'AddressController@show');

        Route::get('address/add', 'AddressController@add');

        Route::post('address/add', 'AddressController@add');

        Route::post('getstate', 'AddressController@getState');

        Route::get('address/edit/{id}', 'AddressController@edit');

        Route::post('address/edit/{id}', 'AddressController@edit');

        Route::post('delete', 'AddressController@delete');
    });
});
Route::get('logout', 'AuthController@logout');

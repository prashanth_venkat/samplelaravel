<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class Address extends Model
{
    /**
     * Constructor to load necessary details
     * 
     * @param mixed $details contains necessary details to load
     */
    public function __construct($details = [])
    {
        $this->details = $details;
    }
    
    /**
     *
     * @var array contains the rules to validate the address 
     */
    private $_rules = [
            'name' => 'bail|required|min:4|max:20',
            'age' => 'required',
            'address' => 'required|min:8|max:120',
            'mobile' => 'required|min:10|max:13',
            'country_id' => 'required',
            'state_id' => 'required',
            'city' => 'required|min:4|max:20',
            ];
    
    /**
     * Retrieve the address list from database
     * 
     * @return json  contains the address list
     */
    
    public function getList()
    {
        $data = DB::table('addresses')
            ->join('countries', 'addresses.country_id', '=', 'countries.country_id')
            ->join('states', 'addresses.state_id', '=', 'states.state_id')
            ->select(
                'addresses.address_id', 'addresses.name', 'addresses.address',
                'addresses.mobile', 'addresses.user_id', 'addresses.city',
                'countries.country_name', 'states.state_name'
            )
            ->where('addresses.user_id', $this->details)
            ->orderBy('addresses.name')
            ->get();
        $list = $data->toArray();
        return json_decode(json_encode($list), true);
    }
    
    /**
     * Store the address details in the database
     * 
     * @param int $userId contains the user id
     * 
     * @return boolean
     */
    public function store($userId)
    {
        $address = $this->details;
        $address = array_slice($address, 1, -1);
        $address['user_id'] = $userId;
        return DB::table('addresses')->insert($address);
    }
    
    /**
     * To get the addres list of the user
     * 
     * @param int $userId contains the user id
     * 
     * @return array $record contains the address list
     */
    public function getAddress($userId)
    {
        $data = DB::table("addresses")
                ->join('countries', 'addresses.country_id', '=', 'countries.country_id')
                ->join('states', 'addresses.state_id', '=', 'states.state_id')
                ->select(
                    'addresses.address_id', 'addresses.name', 'addresses.address',
                    'addresses.mobile', 'addresses.user_id', 'addresses.city',
                    'addresses.age', 'countries.country_name', 'countries.country_id',
                    'states.state_name', 'states.state_id'
                )
                ->where('address_id', $this->details)
                ->where('user_id', $userId)
                ->get();
        $list = $data->toArray();
        return json_decode(json_encode($list), true);
    }
    
    /**
     * Validate the address details
     * 
     * @return mixed
     */
    public function validate()
    {
        $verify = Validator::make($this->details, $this->_rules);
        return !$verify->fails() ? true : $verify->errors(); 
    }
    
    /**
     * Update the address in database
     * 
     * @param int $id     contains the address id
     * @param int $userId contains the user id
     * 
     * @return boolean
     */
    public function updateAddress($id, $userId)
    {
        $address = $this->details;
        $address = array_slice($address, 1, -1);
        $address['user_id'] = $userId;
        return DB::table('addresses')
                ->where('address_id', $id)
                ->update($address);
        
    }
    
    /**
     * Delete the records from the database
     * 
     * @param array $addressId contains the id of addresses 
     * 
     * @return boolean 
     */
    public function deleteAddress($addressId)
    {
        return DB::table('addresses')->whereIn('address_id', explode(',', $addressId))->delete() ? true : false;  
    }
}

<?php

namespace App\Services;

use App\Country;

class CountryList
{
    /**
     * To get the countries list
     * 
     * @return collection contains country details
     */
    public static function getList()
    {
//        $country = new Country();
//        return $country->get();
        return Country::get();
    }
}


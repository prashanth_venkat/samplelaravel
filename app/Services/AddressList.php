<?php

namespace App\Services;

use App\Address;

class AddressList
{
    /**
     * To retrieve all the address book details of user
     * 
     * @param int $userId contains current user id
     * 
     * @return type
     */
    public static function getList($userId)
    {
        $address = new Address($userId);
        return $address->getList();
    }
    /**
     * Retrieve particular address details
     * 
     * @param array $addressId contains address id(s) to be deleted
     * @param int   $userId    contains the currently logged in user id
     * 
     * @return array contains address details
     */
    public static function getAddress($addressId, $userId)
    {
        $address = new Address($addressId);
        return $address->getAddress($userId);
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticable as AuthenticableTrait;

class AuthModel extends Model implements Authenticatable
{
    protected $table = 'users';
    protected $primaryKey = "user_id";
    /**
     *
     * @var array contains rules for validating login details 
     */
    private $_loginRules = [
            'email' => 'required|email|max:64',
            'password' => 'required|min:8|max:20',
            ];
    
    /**
     *
     * @var array contains rules for validating registration details 
     */
    private $_registrationRules = [
        'userName' => 'bail|required|min:4|max:20',
        'email' => 'required|email|max:64',
        'password' => 'required|min:8|max:20|regex:/^(?=.*[0-9])(?=.*[A-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/',
        'password_confirmation' => 'required|same:password',
    ];
    
    /**
     * Constructor to load necessary details
     * 
     * @param mixed $details contains necessary details to load
     */
    public function __construct($details = [])
    {
        $this->details = $details;
    }
    
    /**
     * Store the user details in database
     * 
     * @return boolean $insert
     */
    public function store()
    {
        $user = new User;
        $user->user_name = $this->details['userName'];
        $user->email = $this->details['email'];
        $user->password = bcrypt($this->details['password']);
        $userExist = AuthModel::where('email', $this->details['email'])
                ->get();
        return count($userExist)==0 ? $user->save() : false ;
    }
    
    /**
     * Authenticate the user
     * 
     * @return mixed
     */
    public function verify()
    {
        $user = AuthModel::where('email', $this->details['email'])
                ->get();
        if (!empty($user->toArray())) {
            $userDetails = AuthModel::find($user[0]['user_id']);
            return Hash::check($this->details["password"], ($userDetails->password)) ? $userDetails : false;
        }
        return false;
    }
    
    /**
     * Validate the registration details
     * 
     * @return mixed
     */
    public function validateRegistration()
    {
        $verify = Validator::make($this->details, $this->_registrationRules);
        return !$verify->fails() ? true : $verify->errors() ;
    }
    
    /**
     * Validate the registration details
     * 
     * @return mixed
     */
    public function validateLogin()
    {
        $verify = Validator::make($this->details, $this->_loginRules);
        return !$verify->fails() ? true : $verify->errors() ;
    }
    
    /**
     * Checks if user is already registered
     * 
     * @return boolean
     */
    public function checkExistence()
    {
        return AuthModel::where('email', $this->details)
                ->exists();
    }
    
    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        $id = $this->getAuthIdentifierName();
        return $this->$id;
        
    }

    /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName()
    {
        return $this->getKeyName();
    }

    /**
     * Get the password for the user.
     *
     * @return void 
     */
    public function getAuthPassword()
    {
        
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return void
     */
    public function getRememberToken()
    {
        
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return void 
     */
    public function getRememberTokenName()
    {
        
    }
    
    /**
     * Set the token value for the "remember me" session.
     *
     * @param string $value token value
     * 
     * @return void 
     */
    public function setRememberToken($value) 
    {
        
    }
    
}
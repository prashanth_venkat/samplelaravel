<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    /**
     * To render the required page
     * 
     * @param string $page contains name of the route
     * @param array  $data contains the additional information supplied to the page
     * 
     * @return renders the page as view
     */
    protected function render($page, $data = [])
    {
        return $data ? view($page)
                ->with(array_keys($data)[0], array_values($data)[0])
                ->with(array_keys($data)[1], array_values($data)[1])
                : view($page);
    }
    
    /**
     * Redirects to the page
     * 
     * @param string $page    contains the name of the page
     * @param array  $errors  contains errors if any
     * @param string $message contains message to the page if any
     * 
     * @return redirects to the page
     */
    protected function redirect($page, $errors, $message = "")
    {
        return redirect($page)->with('errors', $errors)->with('message', $message);
    }
}

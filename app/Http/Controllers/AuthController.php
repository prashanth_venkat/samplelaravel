<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticable as AuthenticableTrait;
use App\AuthModel;
use Illuminate\Http\Request;

class AuthController extends Controller
{
//    public function __construct(Request $request) {
//        $this->details = $request->all();
//        $this->method = $request->getMethod();
//    }
    /**
     * Display the login page or address list page
     * 
     * @param Request $request instance of http request
     * 
     * @return renders login/list page
     */
    public function login(Request $request)
    {
        if ($request->isMethod('get')) {
//        if ($this->method == 'GET') {
            return Auth::user() ? $this->redirect("address/list", "") 
                    : $this->render("pages.login");
        } elseif ($request->isMethod('post')) {
//        } elseif ($this->method == 'POST') {
            $details = $request->all();
            $validateDetails = new AuthModel($details);
            $validateResult = $validateDetails->validateLogin();
            if (is_bool($validateResult)) {
                $verifiedUser = $validateDetails->verify();
                if ($verifiedUser) {
                    Auth::login($verifiedUser);
                    return $this->redirect("address/list", "");
                } else {
                    return $this->redirect("login", "", "Email and Password Didn't match");
                }
            } else {
                return $this->redirect("login", $validateResult);
            }
        }
    }
    
    /**
     * Renders Registration/address list page
     * 
     * @param Request $request instance of http request
     * 
     * @return renders the registration/address list page
     */
    public function register(Request $request)
    {
        if ($request->isMethod('get')) {
//        if ($this->method == 'GET') {
            return Auth::user() ? $this->redirect("address/list", "")
                    : $this->render("pages.register");
        } elseif ($request->isMethod('post')) {
//        } elseif ($this->method == 'POST') {
            $details = $request->all();
            $validateDetails = new AuthModel($details);
            $validateResult = $validateDetails->validateRegistration();
            if (is_bool($validateResult)) {
                return $validateDetails->store() ?
                        $this->redirect("login", "", "Login to continue") :
                    $this->redirect("login", "", "Already registered, Login to continue") ;
            } else {
                return $this->redirect('register', $validateResult);
            }
            
        }
    }

    /**
     * Check e-mail availability for registration
     * 
     * @param Request $request instance of http request
     * 
     * @return String
     */
    public function getAvailability(Request $request)
    {
        $existence = new AuthModel($request->input('email'));
        return $existence->checkExistence() ? 'Already Exists' : "Available";
    }

    /**
     * Logs out the user and redirect to login page
     * 
     * @return redirects to login page
     */
    public function logout()
    {
        Auth::logout();
        session()->flush();
        return $this->redirect("login", "");
    }
}
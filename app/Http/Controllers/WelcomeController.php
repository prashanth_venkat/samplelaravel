<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    //
    public function index()
    {
//        return "Welcome to Laravel";
        return view('welcome');
    }
    
    public function contact()
    {
        return view('pages.contact');
    }

    
//    public function store(StoreBlogPost $request)
//    {
//        
//    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function __invoke()
    {
        $first = "Prashanth";
        $last = "Venkat";
        $hobbies = [
            'Video Games',
            'Football',
            'Badminton'
            ];
        return view("pages.about", compact('hobbies', 'first', 'last'));
//        return response('About Mi', 200)
//                ->header("Content-Type", "text/plain");
//        return redirect()->route('about');
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
//use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use App\Address;
use App\Country;
use App\State;
use App\Services\AddressList;
use App\Services\CountryList;
use JWTAuth;

class AddressController extends Controller
{
    /**
     * List address book details
     * 
     * @param Request     $request   contains session data
     * 
     * @return render list page
     */
    public function show(Request $request)
    {
//        $addressList = $addresses->getList(Auth::User()->user_id);
        $addressList = AddressList::getList(Auth::User()->user_id);
        $count = count($addressList);
        $data = array(
            'addressList' => $addressList,
            'count' => $count
        );
        return $this->render("pages.addressList", $data);
    }
    
    /**
     * Display the address add page
     * 
     * @param Request     $request   instance of http request
     * 
     * @return renders the add/list page
     */
    public function add(Request $request)
    {
        if ($request->isMethod('get')) {
//            $countryList = $countries->getList();
            $countryList = CountryList::getList();
            $data = array(
                'countryList' => $countryList,
                '' => '',
            );
            return $this->render("pages.addressAdd", $data);
        } elseif ($request->isMethod('post')) {
            $addressDetails = new Address($request->all());
            $result = $addressDetails->validate();
            if (is_bool($result)) {
                $addressDetails->store(Auth::User()->user_id);
                return $this->redirect("address/list", "");
            } else {
                return $this->redirect("address/add", $result);
            }
        }
    }
    
    /**
     * Ajax call to get the states for respective country selected
     * 
     * @param Request $request instance of http request
     * 
     * @return echoes json $stateList contains state details
     */
    public function getState(Request $request)
    {
//        $state = new State();
        $stateList = State::where('country_id', $request->input('country'))->get();
        echo json_encode($stateList);
    }
    
    /**
     * Display address edit page with selected address details
     * 
     * @param type        $addressId contains the address id
     * @param Request     $request   instance of http request
     * 
     * @return renders the address edit page
     */
    public function edit($addressId, Request $request)
    {
        if ($request->isMethod('get')) {
//            $addressDetails = $addresses->getAddress($addressId, Auth::User()->user_id);
            $addressDetails = AddressList::getAddress($addressId, Auth::User()->user_id);
//            $countryList = $countries->getList();
            $countryList = CountryList::getList();
            $data = array(
                'addressDetails' => $addressDetails,
                'countryList' => $countryList
            );
            return $this->render("pages.addressEdit", $data);
        } elseif ($request->isMethod('post')) {
            $addressDetails = new Address($request->all());
            $result = $addressDetails->validate();
            if (is_bool($result)) {
                $addressDetails->updateAddress($addressId, Auth::User()->user_id);
                return $this->redirect("address/list", "");
            } else {
                return $this->redirect("address/edit/".$addressId, $result);
            }
        }
    }

    /**
     * Delete the selected records
     * 
     * @param Request $request instance of http request
     * 
     * @return type
     */
    public function delete(Request $request)
    {
        $address = new Address();
        $deleteAddress = $address->deleteAddress($request->input('address_id'));
        return json_encode($deleteAddress);
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\AddressList;
use App\Address;
use App\Country;
use App\State;

class AddressListProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\AddressList', function () {
            return new AddressList();
        });
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\CountryList;
use App\Address;
use App\Country;
use App\State;

class CountryListProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\CountryList', function () {
            return new CountryList();
        });
    }
}
